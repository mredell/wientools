# wientools

wientools is a repository of different shell codes and python programs that allow the user to run and process WIEN2k electronic structure calculations.

Here, I am including the different shell scripts I have found most useful in running the calculations as well as some post-processing scripts I have written to plot the DOS and band structure. 

# Run Files

# runwien2k:

This is a shell script to successfully run a calculation on the SLURM workload manager. This assumes that you have done some pen-and-paper configuration math, but will work if done correctly. Additionally, you must have already initialized you system, or else you will need to add that to the bottom of the script prior to the calculation, along with all of the relevant flags (see WIEN2k user's guide for flags) **You also must have loaded WIEN2k in your ~/.bashrc**. The way this should be handled:


*  Identify the number of k-points you have, call it **Nk**

*  Identify how you want mpi to handle the calculation, should be a square number like 1, 4, 16, 64, .... We will call this **Nmpi**. When you decide on this value, it should be set for the variable **nmpi**.

*  Multiply (**Nk** x **Nmpi**) to acquire the total number of processes you will use. This will go in the *--ntasks* flag

*  Now, you need to decide how you want to split up your k-points. This should be done so that each node you will request gets the same number of k-points. For example, if your calculation has 8 different k-points in *case.klist*, you may want to do **kpoints_per_node**=8, 4, 2, or 1, which you will set in the script. Consequently, the number of nodes you request should be (**Nk**/**k_points_per_node**). This number will go in the *--nodes* flag

*  Next, you need to set the number of tasks per node, this will be done by taking (**kpoints_per_node** x **Nmpi**), and should be set in the *--tasks-per-node* flag.

*  Finally, at the bottom of the file should be your run command. This is the typical WIEN2k run command (i.e. run_lapw or runsp_lapw) with all additional flags (-p for parallel, -so for spin-orbit coupling, -orb for LDA+U, etc.)

# plotdos
To successfully run plotdos, one only needs to create input files for the desired plots. The format of these files is as follows:
## sum.txt:
In this file, you will add any columns from the created case.dos{up,dn}. The format should be:

Name1    col1_1    col2_1    col3_1    ...     colN_1

Name2    col1_2    col2_2    col3_2    ...     colN_2
.

.

.

Currently, I have only made it possible to plot more than 1 line. Development on plotting a single line, if desired, is in the works.
The code will sum the columns for each Name.

## sing.txt:
If you want to plot single columns, without adding to other columns, use this format:

Name1   col1

Name2   col2

Name3   col3

.       .

.       .

.       .

This will plot single columns from the output DOS files from WIEN2k.

If you don't want a single column or a summed column plot, simply omit those files and the program will ignore.
