import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
from matplotlib.pyplot import cm
import numpy as np
import os
from os.path import basename

print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
print("Welcome to the WIEN2k DOS plotting software without spin polarization")
print("This software was developed by Matthew Redell at Binghamton University")
print("Summed columns will occur first\nIndividual columns will occur second\n\n")
print("Please make sure you know which columns you would like to plot (Energy in column 1)\nAnd make sure to paste all data for plotting into case.dos(up,dn)\n i.e. paste case.dos1evup case.dos2evup > case.dosup\n\n")
print("Desired summed columns should be listed with name first, followed by the desired columns in sum.txt")
print("Single columns should be listed with name and column in sing.txt")
print("Current capabilities only allow for multiple lines plotted, only plotting 1 line will cause program to fail")
print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")


current_dir = os.getcwd()
case = basename(current_dir)

try:
    dosdata = np.genfromtxt('%s.dos' % case)
except:
    raise Exception("Please paste all relevant DOS files to case.dos using\n $paste -d ' ' *dos{1..n}ev > case.dos")

try:
    data_sum = np.genfromtxt('sum.txt', dtype=str)
    sumfile = True
except:
    sumfile = False

try:
    data_sing = np.genfromtxt('sing.txt', dtype=str)
    singfile = True
except:
    singfile = False

print(sumfile, singfile)

E = dosdata[:,0]

sum_data = []
sum_data_name = []
if sumfile == True:
    for n in range(len(data_sum)):
        sum_data_name.append(data_sum[n][0])
        sum_it = []
        for i in range(1, len(data_sum[n])):
            sum_it.append(int(data_sum[n][i]))
        sum_data_i = np.zeros(len(E))
        for i in sum_it:
            try:
                sum_data_i = sum_data_i + dosdata[:,i-1]
            except:
                raise Exception('You have selected a column (%s) that does not exist' % i)
        sum_data.append(sum_data_i)




plot_columns = []
plot_columns_name = []
if singfile == True:
    for i in range(len(data_sing)):
        column_i = int(data_sing[i][1])
        column_name_i = data_sing[i][0]
        plot_columns_name.append(column_name_i)
        try:
            plot_columns.append(updosdata[:,column_i-1])
        except:
            raise Exception('You have selected a column (%s) that does not exist' % column_i)



xmin = input("Minimum energy (in eV) ")
xmax = input("Maximum energy (in eV) ")

if len(sum_data) != 0:
    sum_file_name = input("Filename for summed plots? (add desired extension) ")
    plt_title = input("Title of plot? ")
    plt.figure()
    color=iter(cm.rainbow(np.linspace(0,1,len(sum_data))))
    for i in range(len(sum_data)):
        c = next(color)
        lab = sum_data_name[i]
        plt.plot(E, sum_data[i], c=c, label=lab)
    plt.xlim(xmin, xmax)
    plt.xlabel('E-E$_f$ (eV)')
    plt.ylabel('States/eV (a.u.)')
    plt.legend()
    plt.title('%s' % plt_title)
    plt.savefig(sum_file_name)

if len(plot_columns) != 0:
    plot_file_name = input("Filename for plot? (add desired extension) ")
    plt_title = input("Title of plot? ")
    plt.figure()
    color=iter(cm.rainbow(np.linspace(0,1,len(plot_columns))))
    for i in range(len(plot_columns)):
        c = next(color)
        lab = plot_columns_name[i]
        plt.plot(E, plot_columns[i], c=c, label=lab)
    plt.xlim(xmin, xmax)
    plt.xlabel('E-E$_f$ (eV)')
    plt.ylabel('States/eV (a.u.)')
    plt.legend()
    plt.title('%s' % plt_title)
    plt.savefig(plot_file_name)

