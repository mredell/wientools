import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
from matplotlib.pyplot import cm
import numpy as np
from scipy.ndimage.filters import gaussian_filter1d
import os
from os.path import basename

print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
print("Welcome to the WIEN2k DOS plotting software with spin polarization")
print("This software was developed by Matthew Redell at Binghamton University")
print("Summed columns will occur first\nIndividual columns will occur second\n\n")
print("Please make sure you know which columns you would like to plot (Energy in column 1)\nAnd make sure to paste all data for plotting into case.dos(up,dn)\n i.e. paste case.dos1evup case.dos2evup > case.dosup\n\n")
print("Desired summed columns should be listed with name first, followed by the desired columns in sum.txt")
print("Single columns should be listed with name and column in sing.txt.")
print("Current capabilities only allow for multiple lines plotted, only plotting 1 line will cause program to fail")
print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

current_dir = os.getcwd()
case = basename(current_dir)

try:
    updosdata = np.genfromtxt('%s.dosup' % case)
except:
    raise Exception("Please paste all relevant DOS files to case.dosup using\n $paste -d ' ' *dos{1..n}evup > case.dosup")
try:
    dndosdata = np.genfromtxt('%s.dosdn' % case)
except:
    raise Exception("Please paste all relevant DOS files to case.dosdn using\n $paste -d ' ' *dos{1..n}evdn > case.dosdn")

try:
    data_sum = np.genfromtxt('sum.txt', dtype=str)
    sumfile = True
except:
    sumfile = False

try:
    data_sing = np.genfromtxt('sing.txt', dtype=str)
    singfile = True
except:
    singfile = False

Eup = updosdata[:,0]
Edn = dndosdata[:,0]

upsum_data = []
dnsum_data = []
sum_data_name = []
if sumfile == True:
    for n in range(len(data_sum)):
        sum_data_name.append(data_sum[n][0])
        sum_it = []
        for i in range(1, len(data_sum[n])):
            sum_it.append(int(data_sum[n][i]))
        sum_data_iup = np.zeros(len(Eup))
        sum_data_idn = np.zeros(len(Eup))
        for i in sum_it:
            try:
                sum_data_iup = sum_data_iup + updosdata[:,i-1]
                sum_data_idn = sum_data_idn + dndosdata[:,i-1]
            except:
                raise Exception('You have selected a column (%s) that does not exist' % i)
        upsum_data.append(sum_data_iup)
        dnsum_data.append(sum_data_idn)


upplot_columns = []
dnplot_columns = []
plot_columns_name = []
if singfile == True:
    for i in range(len(data_sing)):
        column_i = int(data_sing[i][1])
        column_name_i = data_sing[i][0]
        plot_columns_name.append(column_name_i)
        try:
            upplot_columns.append(updosdata[:,column_i-1])
            dnplot_columns.append(dndosdata[:,column_i-1])
        except:
            raise Exception('You have selected a column (%s) that does not exist' % column_i)


xmin = float(input("Minimum energy (in eV) "))
xmax = float(input("Maximum energy (in eV) "))

if len(upsum_data) != 0:
    sum_file_name = input("Filename for summed plots? (add desired extension) ")
    plt_title = input("Title of plot? ")
    plt.figure()
    color=iter(cm.rainbow(np.linspace(0,1,len(upsum_data))))
    for i in range(len(upsum_data)):
        c = next(color)
        lab = sum_data_name[i]
        plt.plot(Eup, upsum_data[i], c=c, label=lab)
        plt.plot(Edn, -dnsum_data[i], c=c, label='_nolegend_')
    plt.xlim(xmin, xmax)
    #plt.ylim(ymin, ymax)
    plt.xlabel('E-E$_f$ (eV)')
    plt.ylabel('States/eV (a.u.)')
    plt.legend()
    plt.title('%s' % plt_title)
    plt.savefig(sum_file_name)

if len(upplot_columns) != 0:
    plot_file_name = input("Filename for plot? (add desired extension) ")
    plt_title = input("Title of plot? ")
    plt.figure()
    color=iter(cm.rainbow(np.linspace(0,1,len(upplot_columns))))
    for i in range(len(upplot_columns)):
        c = next(color)
        lab = plot_columns_name[i]
        #smoothup = gaussian_filter1d(upplot_columns[i], sigma=2)
        #smoothdn = gaussian_filter1d(dnplot_columns[i], sigma=2)
        plt.plot(Eup, upplot_columns[i], c=c, label=lab)
        plt.plot(Edn, -dnplot_columns[i], c=c, label='_nolegend_')
    plt.xlim(xmin, xmax)
    #plt.ylim(ymin, ymax)
    plt.xlabel('E-E$_f$ (eV)')
    plt.ylabel('States/eV (a.u.)')
    plt.legend()
    plt.title('%s' % plt_title)
    plt.savefig(plot_file_name)

