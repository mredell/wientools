#!/bin/bash

dir=$(basename "$PWD")
echo 'Number of case.dosev files'

numfiles=
read varname
numfiles+=$varname

echo 'Would you like to plot spin-polarized DOS? (y/n)'

spin=
read varname
spin+=$varname

if [ "$spin" = "y" ]; then
    rm $dir.dosup
    rm $dir.dosdn
    upfiles=''
    dnfiles=''
    for i in $(seq 1 $numfiles); do
        upfiles+="$dir.dos"$i"evup "
        dnfiles+="$dir.dos"$i"evdn "
    done
    paste -d ' ' $upfiles > $dir.dosup
    paste -d ' ' $dnfiles > $dir.dosdn
    python ~/wientools/dos/spdosplot.py
else
    rm $dir.dos
    files=''
    for i in $(seq 1 $numfiles); do
        files+="$dir.dos"$i"ev "
    done
    paste -d ' ' $files > $dir.dos
    python ~/wientools/dos/dosplot.py
fi
